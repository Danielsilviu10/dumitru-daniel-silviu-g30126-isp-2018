package problema8;

public class Rectangle{
	
	private double width = 1.0;
	private double length = 1.0;
	
	public Rectangle() {
		
	}
	
	public Rectangle(double width,double length) {
		this.setLength(length);
		this.setWidth(width);
	}
	
	public Rectangle(double width) {
		
		this.width = width;
	}
	
	public Rectangle(double width,double length,String color,boolean filled) {
		
		this.setWidth(width);
		this.setLength(length);
	
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

}
