package Laborator4;

public class Book6 {

	private String name;
    private Author[] A=new Author[3];
    private int numAuthors = 0;
    private double price;
    private int qtyInStock=0;
    
    public Book6 (String n , Author[] authors,double p ) {
    	name=n;
    	A=authors;
    	price=p;
    	
	}
    public Book6 (String n , Author[] authors,double p , int qty ){
    	
    	name=n;
    	A=authors;
        price=p;
       qtyInStock=qty;
    }
    public String getName() {
    	return name;
   }
    public Author[] getAuthors() {
    	return A;
    }
    public double getPrice() {
    	return price;
    }
    public void setPrice(double p) {
    	this.price=p;
    }
    public int getQtyInStock() {
    	return qtyInStock;
    }
    public void setQtyInStock(int qty) {
    qtyInStock=qty;	
    }
    public String toString() {
    	return getName()+ "is written by " + numAuthors + " authors ";
    }
    public void printAuthors() {
       for (int i=0;i<= numAuthors;i++) {
    	   System.out.println(A[i]);
       }
    	
    }
    
    }
