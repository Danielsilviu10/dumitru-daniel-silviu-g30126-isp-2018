package lab3tema;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;

public class Lab3_3 {

	public static void main(String[] args) {
		
		City ny = new City();
		Robot robocop = new Robot(ny,1,1,Direction.NORTH);
		robocop.move();
		robocop.move();
		robocop.move();
		robocop.move();
		robocop.move();
		robocop.turnLeft();
		robocop.turnLeft();
		robocop.move();
		robocop.move();
		robocop.move();
		robocop.move();
		robocop.move();
		
	}

}
