package lab3tema;
import static java.lang.Math.*;

public class MyPoint {
	
	private int x;
	private int y;
	
	public MyPoint () {
		  
	}
	public MyPoint(int x, int y) {
		
		this.setX(x);
		this.setY(y);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void SetXY(int x,int y) {
		
		this.x = x;
		this.y = y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public String toString() {
		
		return "x:" + this.x + "y: " + this.y;
	}
	
	public double distanceXY(int x,int y) {
		
		return sqrt(pow(this.x-x,2)+pow(this.y-y,2));
	}
	
	public double distanceXY(MyPoint p) {
		
		return sqrt(pow(this.x-p.x,2)+pow(this.y-p.y,2));
		
	}
	
	

}
